package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> Spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.Spells = spells;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for(Spell spell: Spells){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for(int i = Spells.size()-1;i>=0;i--){
            Spells.get(i).undo();
        }
    }
}
